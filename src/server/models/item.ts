import {Schema, Document, Model, model} from "mongoose";
import {iItem} from "../../types/modelTypes";

const itemSchema : Schema<iItem> = new Schema<iItem>({
    name: {type: String},
    description: {type: String, default: "No description available"},
    value: {type: Number, min: 0, default: 1},
});

export const Item: Model<iItem> = model<iItem>("Item", itemSchema);