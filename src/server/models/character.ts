import {Schema, Document, Model, model, Types} from "mongoose";
import {iCharacter} from "../../types/modelTypes";

const validateName = (name: string): boolean => {

    const nameRegex = /^[a-zA-Z0-9]+ [a-zA-Z0-9]* [a-zA-Z0-9]*$/;

    return name.length > 5 && nameRegex.test(name) && name.length < 30;

};

const characterSchema : Schema<iCharacter> = new Schema<iCharacter>({
    userId: {type: Types.ObjectId},
    created: {type: Date, default: Date.now},
    name: {type: String, validate: {validator: validateName, message: "Invalid name. Name must be between " +
                "5 and 30 characters, and only contain English letters and numbers. Up to 2 spaces are allowed"}},
    money: {type: Number, min: 0, default: 0},
    mainHand: {type: Types.ObjectId, default: null}, //If manually set to null on request, strongParams will reject it
    offHand: {type: Types.ObjectId, default: null},  //because that shouldn't happen. It would be preferable if I could
    armor: {type: Types.ObjectId, default: null},    //set a default equipment called "nothing", but that would be a
    ring: {type: Types.ObjectId, default: null}     //headache for unit tests and my professor trying to test it
});

export const Character: Model<iCharacter> = model<iCharacter>("Character", characterSchema);