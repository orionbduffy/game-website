import {Schema, Document, Model, model, Types} from "mongoose";
import {iSession} from "../../types/modelTypes";

const validateSessionId = (sessionId: string): boolean => {

    const sessionIdRegex = /^[a-zA-Z0-9\-_]*$/;

    return sessionIdRegex.test(sessionId) && sessionId.length == 24;

};

const sessionSchema : Schema<iSession> = new Schema<iSession>({
    sessionId: {type: String, validate: {validator: validateSessionId, message: "Invalid sessionId. sessionId must be " +
                "24 characters, and only contain English letters and numbers, as well as dashes and underscores."}},
    userId: {type: Types.ObjectId},
    created: {type: Date, expires: "7d", default: Date.now}
});

export const Session: Model<iSession> = model<iSession>("Session", sessionSchema);