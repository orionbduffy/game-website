import {Schema, Document, Model, model} from "mongoose";
import {iEquipment} from "../../types/modelTypes";


const equipmentSchema : Schema<iEquipment> = new Schema<iEquipment>({
    name: {type: String},
    description: {type: String, default: "No description available"},
    value: {type: Number, min: 0, default: 1},
    attack: {type: Number, default: 0},
    defense: {type: Number, default: 0},
    slot: {type: Number, min: [0, 'Below equipment slot range'], max: [3, 'Above equipment slot range']}
});

equipmentSchema.virtual('slotType').get(function (this: iEquipment) {

    switch (this.slot) {
        case 0:
            return 'mainHand';
        case 1:
            return 'offHand';
        case 2:
            return 'armor';
        case 3:
            return 'ring';
    }

}).set(function (this: iEquipment, slot: string) {

    switch (slot) {
        case 'mainHand':
            this.slot = 0;
            break;
        case 'offHand':
            this.slot = 1;
            break;
        case 'armor':
            this.slot = 2;
            break;
        case 'ring':
            this.slot = 3;
            break;
        default:
            throw new Error('Not a valid slot type');
    }
});

export const Equipment: Model<iEquipment> = model<iEquipment>("Equipment", equipmentSchema);