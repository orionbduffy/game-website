import {Schema, Document, Model, model} from "mongoose";
import {iUser} from "../../types/modelTypes";

const validateEmail = (email: string): boolean => {

    const emailRegex = /^\S+@\S+\.\S+$/;

    return email.length > 5 && emailRegex.test(email) && email.length < 30;

};

const validateUsername = (username: string): boolean => {

    const usernameRegex = /^[a-zA-Z0-9]*$/;

    return username.length >= 5 && usernameRegex.test(username) && username.length <= 30;

};

const validatePassword = (password: string): boolean => {

    const passwordRegex = /^\$\S{1,2}\$[0-9]{2}\$[.\/A-Za-z0-9]{53}$/;

    return passwordRegex.test(password);

};

const userSchema : Schema<iUser> = new Schema<iUser>({
    registered: {type: Date, default: Date.now},
    email: {type: String, validate: {validator: validateEmail, message: "Invalid email."}},
    username: {type: String, validate: {validator: validateUsername, message: "Invalid username. Username must be between " +
                "5 and 30 characters, and only contain English letters and numbers."}},
    password: {type: String, validate: {validator: validatePassword, message: "Invalid password. Password must be a Bcrypt hash."}},
    premiumCurrency: {type: Number, min: 0, default: 0}
});

export const User: Model<iUser> = model<iUser>("User", userSchema);