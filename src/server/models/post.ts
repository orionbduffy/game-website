import {Schema, Document, Model, model} from "mongoose";
import {iPost} from "../../types/modelTypes";

const postSchema : Schema<iPost> = new Schema<iPost>({
    postDate: {type: Date, default: Date.now()},
    chatChannel: {type: Number},
    message: {type: String},
});

export const Post: Model<iPost> = model<iPost>("Post", postSchema);