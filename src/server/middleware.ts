import {Request, Response, NextFunction} from "express";
import {Session} from "./models/session";
import {User} from "./models/user";

export const authenticate = async (request: Request, response: Response, next: NextFunction) =>
{
    const {sessionId} = request.cookies;
    console.log("a request to authenticate a user with session ID " + sessionId + " came in");
    const userSession = await Session.findOne({sessionId: {$eq: sessionId}});
    const user = await User.findOne(
        {_id: {$eq: userSession?.userId}},
        ["emailAddress", "username", "creationDate"]
    );
    if (user) {
        console.log("user authenticated");
        response.locals.user = user;
        return next();
    } else {
        console.log("user not authenticated");
        return response.sendStatus(401);
    }
};

export const strongParams = (params: {[key: string]: string}) =>
{
    return (request: Request, response: Response, next: NextFunction) =>
    {
        const strongParams: {[key: string]: any} = {};
        const weakParams = request.body;
        request.body = null;

        Object.entries(params).forEach(([key, value]) =>
        {
            const weakParam = weakParams[key];
            if(value.trim().toLowerCase() !=='array')
            {
                if (weakParam !== undefined && weakParam !== null && typeof weakParam === value) {
                    strongParams[key] = weakParam;
                } else {
                    return response.sendStatus(400);
                }
            } else
            {
                if(weakParam !== undefined && weakParam !== null && Array.isArray(weakParam) === true)
                {
                    strongParams[key] = weakParam;
                } else {
                    return response.sendStatus(400);
                }
            }
        });

        if(strongParams !== {})
        {
            response.locals.strongParams = strongParams;

            return next();
        } else
        {
            return response.sendStatus(400);
        }
    }
};