import Express from "express";
import Mongoose from "mongoose";
import Bcrypt from "bcrypt";
import Nanoid from "nanoid";
import CookieParser from "cookie-parser";
import {User} from "./models/user";
import {Session} from "./models/session";
import {iUser,iSession, iCharacter} from "../types/modelTypes";
import path from 'path';
import {authenticate, strongParams} from "./middleware";
import {webSocket} from './webSocket';

const gameServer = Express();
const clientAppDirectory = path.join(__dirname, '..', 'client');

Mongoose.connect(process.env.DATABASE_URL || "mongodb://localhost/game", {useNewUrlParser: true, useUnifiedTopology: true}).then(r => console.log(" "));

Mongoose.connection.once("open", () => console.log("connected to database successfully."));

gameServer.use(Express.json());
gameServer.use(CookieParser(process.env.COOKIE_SECRET));
gameServer.use(Express.static(clientAppDirectory));

webSocket();

const clearExcessSessions = async (user: iUser) =>
{
    let sessions = await Session.find({userId: {$eq: user._id}}).sort({date: 1});
    console.log("User has "+ sessions.length + " sessions");
    while (sessions.length > 5) {
        await Session.deleteOne({sessionId: {$eq: sessions[0].sessionId}});
        sessions = await Session.find({userId: {$eq: user._id}}).sort({date: 1});
    }
};

gameServer.post("/api/users",
    [strongParams({email: 'string', username: 'string', password: 'string'})],
    async (request: Express.Request, response: Express.Response) =>
    {
    const {email, username, password} = response.locals.strongParams;
    console.log("A request to create a user came in with the username: " + username + " and email: " + email);


    const emailExists = await User.findOne({email: {$eq: email}});
    if (emailExists){
        console.log("A user with the email address: " + email + " already exists. The request will be rejected.");
        return response.sendStatus(400);
    }

    const usernameExists = await User.findOne({username: {$eq: username}});
    if (usernameExists){
        console.log("A user with the username: " + username + " already exists. The request will be rejected.");
        return response.sendStatus(400);
    }

    Bcrypt.hash(password, 12, async (error, hash) => {
        if (error){
            console.log("An error occurred while hashing the password.");
            return response.sendStatus(400);
        }

        const newUser = await User.create({
            email: email,
            username: username,
            password: hash
        });

        console.log("The user: " + newUser.email + " has been created.");
        return response.sendStatus(200);
    });
});

gameServer.get("/api/user/", async (request: Express.Request, response: Express.Response) => {
    const {sessionId} = request.cookies;
    if (!sessionId){
        return;
    }
    const userSession = await Session.findOne({ sessionId: {$eq: sessionId } });
    if (userSession) {
        const user = await User.findOne(
            {_id: {$eq: userSession.userId}},
            {"email": 1, "username": 1, "creationDate": 1, "_id": 0}
        );
        return response.send(user).status(200);
    } else {
        return response.clearCookie("Session");
    }
});

gameServer.post("/api/sessions",
    [strongParams({email: 'string', password: 'string'})],
    async (request: Express.Request, response: Express.Response) => {
    console.log("An end user is attempting to login.");
    const {email, password} = response.locals.strongParams;
    const user = await User.findOne({ email: { $eq: email } });

    if (!user){
        console.log("No user was found with the email address: " + email);
        return response.sendStatus(400);
    }

    Bcrypt.compare(password, user.password, (error, result) => {

        if (error){
            console.log("An error occurred while checking the user's password hash.");
            return response.sendStatus(400);
        }

        if (result === true){
            console.log("User: " + user.email + " has logged in.");
            const sessionId = Nanoid(24);
            Session.create({
                sessionId: sessionId,
                userId: user._id
            });

            clearExcessSessions(user);

            response.cookie("sessionId", sessionId, {httpOnly: true});
            return response.sendStatus(200);
        }

        return response.sendStatus(400);
    });
});

gameServer.delete("/api/sessions/", async (request: Express.Request, response: Express.Response) => {
    const sessionCookie = request.headers.cookie?.split("sessionId=");
    if(sessionCookie) {
        const sessionId = sessionCookie[1];
        await Session.deleteOne({sessionId: {$eq: sessionId}});
        response.clearCookie("Session");
        console.log("The user has logged out.");
        return response.sendStatus(200);
    }
});

gameServer.get('/*', (request: Express.Request, response: Express.Response) => {

    const indexPath = path.join(clientAppDirectory, 'index.html');

    return response.sendFile(indexPath);
});

const port =  5000;
gameServer.listen(process.env.PORT || port, () => console.log("The server has started on localhost:" + port));