import WebSocket from 'ws';
import Mongoose from 'mongoose';
import {Post} from './models/post';

export const webSocket = function () {
    const port: number = <number><unknown>process.env.WS_PORT || 5001;

// Instantiate a new web socket server on localhost with port 5000
    const webSocketServer: WebSocket.Server = new WebSocket.Server({port});

// Called when a client connects to the websocket (kind of similar to a route handler)
    webSocketServer.on('connection', (webSocketClient: WebSocket, request: Express.Request) => {

        console.log(Date() + 'A client has attempted to connect to the websocket server');

        // Send the connected client a message right away just to say hello

        // Set up a callback to listen for messages that the newly connect client sends through their websocket
        webSocketClient.on('message', async (message) => {

            console.log(Date() + 'Received message from client: ', message);

                const messageObject = JSON.parse(<string>message);

                const newMessage = await Post.create({
                    postDate: Date.now(), //Seems to be needed to send the date to the client
                    chatChannel: messageObject.chatChannel,
                    message: messageObject.message
                });

            // The websocket server tracks all clients that connected to it, iterate through them to send a broadcast
            webSocketServer.clients.forEach((client) => {

                // Make sure the client is still connected before trying to send a message to them
                if (client.readyState === WebSocket.OPEN) {

                    // Broadcast the clients message to all other connected clients (like a chat room)
                    client.send(JSON.stringify(newMessage));

                }

            });

            console.log('Broadcasted message to all connected clients!');

        });

    });

    console.log('Websocket server is up and ready for connections on port', port);
};