import React from 'react';
import TextField from "@material-ui/core/TextField";
import {RouteComponentProps} from "react-router";

interface usernameInputProps{
    username: string,
    onUsernameChange(event: React.ChangeEvent<HTMLInputElement>): void
}

function UsernameInput (props:usernameInputProps) {

        const {username, onUsernameChange} = props;
        return <TextField label = "Username" value = { username } onChange = { onUsernameChange } placeholder = "example" />;

}

export default UsernameInput;