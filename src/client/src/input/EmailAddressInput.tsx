import React from 'react';
import TextField from "@material-ui/core/TextField";
import {RouteComponentProps} from "react-router";

interface emailInputProps{
    emailAddress: string,
    onEmailChange(event: React.ChangeEvent<HTMLInputElement>): void
}
function EmailAddressInput (props: emailInputProps){

        const {emailAddress, onEmailChange} = props;

        return <TextField label = "Email Address" value = { emailAddress } onChange = { onEmailChange } placeholder = "example@example.com"/>;

}

export default EmailAddressInput;