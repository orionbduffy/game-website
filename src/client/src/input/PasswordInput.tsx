import React from 'react';
import TextField from "@material-ui/core/TextField";
import {RouteComponentProps} from "react-router";

interface passwordInputProps{
    password: string,
    onPasswordChange(event: React.ChangeEvent<HTMLInputElement>): void
}

function PasswordInput (props: passwordInputProps) {

        const {password, onPasswordChange} = props;
        return <TextField label = "Password" value = { password } onChange = { onPasswordChange } type = "password" />;


}

export default PasswordInput;