import {iUser} from "../../../../types/modelTypes";
// This describes what fields our username state will have
export interface IUserState {

    user?: iUser,
    loggedIn: boolean

}