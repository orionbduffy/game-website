import React from 'react';
import Axios from 'axios';
import '../styles/App.css';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import NavigationBar from "./NavigationBar";
import {useDispatch, useSelector} from "react-redux";
import {IRootReducer} from "../redux/IRootReducer";
import {iUser} from "../../../types/modelTypes";
import {loginUser, logoutUser} from "../redux/actions/UserActions";
import HomePage from "../pages/HomePage";
import LoginPage from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";

//"postbuild": "del /S ..\\..\\build\\client\\ && rmvdir ..\\..\\build\\client\\static && move .\\build\\* ..\\..\\build\\client",

function App() {

  const [emailAddress, setEmailAddress] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [errorVisible, setErrorVisibility] = React.useState(false);

  const dispatch = useDispatch();

  // Hook used to pick a specific state out of the Redux store
  // Note: The selector is given the root state, we must dig down to the specific property we care about
  const user: iUser | undefined = useSelector<IRootReducer, iUser | undefined>(
      state => state.userReducer.user);

  function handleEmailAddressChange(event: React.ChangeEvent<HTMLInputElement>) {
    const newEmailAddress = event.target.value;
    setEmailAddress(() => newEmailAddress);
  }

  function handlePasswordChange(event: React.ChangeEvent<HTMLInputElement>) {
    const newPassword = event.target.value;
    setPassword(() => newPassword);
  }

  async function fetchUser(){
    const response = await Axios.get("/api/user/", {withCredentials: true});
    const { data } = response;

    if(data)
      dispatch(loginUser(data));

    setEmailAddress(() => "");
    setPassword(() => "");
  }

  async function onLoginClick(){
    const credentials = {email: emailAddress, password};
    try {

      await Axios.post("/api/sessions", credentials);
      await fetchUser();

    } catch (error) {
      setErrorVisibility(() => true);
    }
  }

  function onLogoutClick(){
    Axios.delete("/api/sessions", {withCredentials:true});
    dispatch(logoutUser());
  }

  React.useEffect(() => {
    (async () => await fetchUser())();
  }, []);

  return (
    <div className="App">
      <BrowserRouter>
        <NavigationBar onLogout = {onLogoutClick}/>
        <Switch>
          <Route path = "/login">
            <LoginPage
                emailAddress = {emailAddress}
                password = {password}
                onEmailAddressChange = {handleEmailAddressChange}
                onPasswordChange = {handlePasswordChange}
                onLoginClick = {onLoginClick}
                errorVisible = {errorVisible}
            />
          </Route>
          <Route path = "/register">
            <RegisterPage />
          </Route>
          <Route path = "/home">
            <HomePage/>
          </Route>
          <Redirect exact from = "/" to = "/home" />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
