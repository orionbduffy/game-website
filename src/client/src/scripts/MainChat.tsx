import React from 'react';
import { Input, Button }  from "@material-ui/core";
import {iPost, iUser} from "../../../types/modelTypes";
import {useSelector} from "react-redux";
import {IRootReducer} from "../redux/IRootReducer";

// This is a functional component, it's simply a function that returns what to render
function MainChat() {

    const loggedIn: boolean = useSelector<IRootReducer, boolean>(
        state => state.userReducer.loggedIn);

    // React.useState() returns a two item list, the state itself and a function used to set the state
    // Here, we use a state for the websocket itself with an initial state of 'null'
    const [webSocket, setWebSocket] = React.useState<WebSocket | null>(null);

    // Use another state to track all messages received by the websocket with an initial state of an empty array
    const [messages1, setMessages1] = React.useState<iPost[]>([]);
    const [messages2, setMessages2] = React.useState<iPost[]>([]);

    // Use another state for the message being typed in the input component, the initial state is an empty string
    const [messageToSend, setMessageToSend] = React.useState('');

    // React.useEffect(), as it is used here, runs a function after the component mounts.
    // Here, we use an effect callback to instantiate a websocket connection (kind of like making an API request)
    React.useEffect(() => {

        // Instantiate the websocket on port 5000
        const webSocket: WebSocket = new WebSocket('ws://localhost:5001');

        // Set the websocket state (see line 10)
        setWebSocket(webSocket);

        // When we receive a message, meaning the server sent us a message, add it to the received messages state
        webSocket.onmessage = (message) => {

            console.log('Received message: ', message);

            const messageObject = JSON.parse(message.data);

            console.log("Parsed message object: ", messageObject);
            // Set state calls are batched, this means they are not guaranteed to capture every intermediary change.
            // Passing a function to set state ensures we set states in successive order (we'll talk more about this later)
            if(messageObject.chatChannel === 1)
                setMessages1((oldMessages) => [...oldMessages, messageObject]);
            else if (messageObject.chatChannel === 2)
                setMessages2((oldMessages) => [...oldMessages, messageObject]);

        };

        // Basically the functional equivalent to componentWillUnmount, clean up the websocket when the component is unmounted
        return () => webSocket.close();

        // Don't worry about the empty array here, we'll talk about it later
        // For the curious, it is used to specify variable dependencies for the effect to make it trigger (we have no dependencies here)
    }, []);

    // Set up a function to call when the user presses the send button
    function sendMessage(chatNum: number) {

        // Don't bother trying to send a message to a null or closed websocket
        if (webSocket && webSocket?.readyState === WebSocket.OPEN) {

            const messageObject = {
                chatChannel: chatNum,
                message: messageToSend
            };
            // Send the currently typed message to the websocket
            webSocket.send(JSON.stringify(messageObject));

            console.log('Sent message to websocket: ', messageToSend);

        }

    }

    function convertDate(dateString: string | Date)
    {
        const dateObject = new Date(dateString);
        return dateObject.toLocaleTimeString();
    }

    function allowChat()
    {
        if(loggedIn)
            return (
                <div>
                    <Input
                        value={messageToSend}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => setMessageToSend(event.target.value)}
                        name={'chat1Input'}
                    />

                    <Button onClick={() => sendMessage(1)}>
                        Send Message
                    </Button>
                </div>
            );
        else
            return (
                <div>
                <h4>You must be logged in to send messages!</h4>
                </div>
            );
    }

    return (
        <div>
            <div style={{display: 'flex'}}>
                <div style={{ height: '100vh', width: '50%', backgroundColor: 'lightgray'}}>

                    <h1>Main Chat</h1>

                    {allowChat()}

                    {
                        // Create H1 components from each message the server sent back
                        // DONT EVER SET A LIST KEY AS AN INDEX (I'm only doing it here because we don't have a unique value to key off of)
                        messages1.map((message) => {

                            return (

                                <h5 key={message._id}>

                                    {convertDate(message.postDate)}: {message.message}

                                </h5>
                            )
                        })

                    }

                </div>


            </div>
        </div>
    );
}

export default MainChat;