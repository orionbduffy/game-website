import React from 'react';
import Button from "@material-ui/core/Button";
import {RouteComponentProps, withRouter} from 'react-router-dom';
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import AppBar from "@material-ui/core/AppBar";
import {iUser} from "../../../types/modelTypes";
import {useSelector} from "react-redux";
import {IRootReducer} from "../redux/IRootReducer";

interface navBarProp extends RouteComponentProps<any>{
    onLogout(): void
}

function NavigationBar(props: navBarProp) {

    const user: iUser | undefined = useSelector<IRootReducer, iUser | undefined>(
        state => state.userReducer.user);
    const loggedIn: boolean = useSelector<IRootReducer, boolean>(
        state => state.userReducer.loggedIn);

     function loggedInNavBar(){
        const name = user?.username;

        return (

            <AppBar position={"static"}>

            <Toolbar>

                <IconButton edge={"start"} color={"inherit"}>
            <AccountBoxIcon />
            </IconButton>

        { /* ToolBar uses a flex layout so we can use flexGrow here to ensure it pushes buttons all the way to the right */ }
        <Typography variant={"h6"} style={{ flexGrow: 1 }}>
        Welcome {name}
        </Typography>

        <Button color={"inherit"}
        onClick={handleHomePageNavigation}
        style={{ marginRight: '1rem' }}>
        Home
        </Button>

        <Button color={"inherit"}
        onClick={handleLogoutPageNavigation}>
            Logout
            </Button>

            </Toolbar>
            </AppBar>
    );
    }

    function loggedOutNavBar(){
        return (

            <AppBar position={"static"}>

            <Toolbar>

                <Typography variant={"h6"} style={{ flexGrow: 1 }}>
        Welcome to my portfolio!
        </Typography>

        <Button color={"inherit"}
        onClick={handleHomePageNavigation}
        style={{ marginRight: '1rem' }}>
        Home
        </Button>

        <Button color={"inherit"}
        onClick={handleLoginPageNavigation}>
            Login
            </Button>

            <Button color={"inherit"}
        onClick={handleRegisterPageNavigation}>
            Register
            </Button>

            </Toolbar>
            </AppBar>
    );
    }


    function handleHomePageNavigation() {
        const {history} = props;
        history.push('/home');
    }

    function handleLoginPageNavigation() {
        const {history} = props;
        history.push('/login');
    }

    function handleLogoutPageNavigation() {
        const {history, onLogout} = props;
        onLogout();
        history.push('/home');
    }

    function handleRegisterPageNavigation() {
        const {history} = props;
        history.push('/register');
    }

    if (loggedIn){
        return loggedInNavBar();
    }
    else {
        return loggedOutNavBar();
    }

        // return (
        //     <Router>
        //     <div>
        //         <BottomNavigation
        //             value={value}
        //             onChange={(event, newValue) => {
        //                 setValue(newValue);
        //             }}
        //             showLabels
        //             className={classes.root}
        //         >
        //             <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
        //             <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
        //             <BottomNavigationAction label="Nearby" icon={<LocationOnIcon />} />
        //         </BottomNavigation>
        //         <Button variant="contained" href="/register" color = "primary" className="">
        //             Register
        //         </Button>
        //         <Link to = "/register"> Register </Link>
        //     </div>
        //     </Router>
        // );

}


export default withRouter(NavigationBar);