import React from 'react';
import {withRouter} from 'react-router-dom';
import '../styles/HomePage.css';
import {useSelector} from "react-redux";
import {IRootReducer} from "../redux/IRootReducer";
import MainChat from "../scripts/MainChat";

function HomePage (){

        return(
            <div className={"mainDiv"}>
                <h2 style={{ textAlign: 'center' }}>Home Page</h2>
                <div className={"secondaryDiv"}>
                    <div className={"tertiaryDiv"}>
                        <h3 style={{ textAlign: 'center' }}>Game</h3>
                        <p>
                            Sorry, the game does not exist right now, this is currently just a mock-up of the website. As apology, here is the same meme image I put in the website I copied this stuff over from.
                        </p>
                        <img src = {require("../imgs/PrincessBrideBestMovie.jpg")} alt = "The Princess Bride is an amazing movie"/>
                    </div>
                    <div className={"tertiaryDiv"}>
                        <h3 style={{ textAlign: 'center' }}>Chat</h3>
                        <MainChat />
                    </div>
                </div>
            </div>
        );
}

export default withRouter(HomePage);