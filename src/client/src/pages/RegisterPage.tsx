import React from 'react';
import EmailAddressInput from '../input/EmailAddressInput';
import PasswordInput from '../input/PasswordInput';
import UsernameInput from '../input/UsernameInput';
import Button from "@material-ui/core/Button";
import {Redirect} from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import '../styles/LoginPage.css';
import Axios from "axios";

function RegisterPage() {

    const [emailAddress, setEmailAddress] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [username, setUsername] = React.useState('');
    const [redirectToLogin, setRedirectToLogin] = React.useState(false);

    function handleEmailAddressChange(event: React.ChangeEvent<HTMLInputElement>) {
        const newEmailAddress = event.target.value;
        setEmailAddress(() => newEmailAddress);
    }

    function handlePasswordChange(event: React.ChangeEvent<HTMLInputElement>) {
        const newPassword = event.target.value;
        setPassword(() => newPassword);
    }

    function handleUsernameChange(event: React.ChangeEvent<HTMLInputElement>) {
        const newUsername = event.target.value;
        setUsername(() => newUsername);
    }

    async function onRegisterClick() {
        const data = {email: emailAddress, username, password};
        try {

            await Axios.post("/api/users", data);

            setRedirectToLogin(() => true);
            setUsername(() => '');
            setEmailAddress(() => '');
            setPassword(() => '');

        } catch (error) {

            console.error(error.message);
        }
    }

    function redirect()
    {
        if (redirectToLogin) {
            return <Redirect to="/login"/>;
        }
    }

    return (
        <div className="LoginInputContainer">
            <div className="Inputs">
                <EmailAddressInput emailAddress={emailAddress} onEmailChange={handleEmailAddressChange}/>
            </div>
            <div className="Inputs">
                <UsernameInput username={username} onUsernameChange={handleUsernameChange}/>
            </div>
            <div className="Inputs">
                <PasswordInput password={password} onPasswordChange={handlePasswordChange}/>
            </div>
            <div className="Inputs">
                <Button variant="contained" onClick={onRegisterClick}> Register </Button>
            </div>
            {redirect()}
        </div>
    );
}

export default withRouter(RegisterPage);