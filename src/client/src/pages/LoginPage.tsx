import React from 'react';
import EmailAddressInput from '../input/EmailAddressInput';
import PasswordInput from '../input/PasswordInput';
import Button from "@material-ui/core/Button";
import {RouteComponentProps, withRouter} from 'react-router';
import {Link, Redirect} from 'react-router-dom';
import '../styles/LoginPage.css';
import {CSSProperties} from "@material-ui/styles";
import {useSelector} from "react-redux";
import {IRootReducer} from "../redux/IRootReducer";

interface loginPageProps extends RouteComponentProps<any> {
    emailAddress: string,
    password: string,

    onEmailAddressChange(event: React.ChangeEvent<HTMLInputElement>): void,

    onPasswordChange(event: React.ChangeEvent<HTMLInputElement>): void,

    onLoginClick(): void,

    errorVisible: boolean
}

function LoginPage(props: loginPageProps) {

    const loggedIn: boolean = useSelector<IRootReducer, boolean>(
        state => state.userReducer.loggedIn);

    const {emailAddress, password, onEmailAddressChange, onPasswordChange, onLoginClick, errorVisible} = props;

    function redirect()
    {
        if (loggedIn) {
            return <Redirect to="/home"/>
        }
    }

    const errorStyle = {
        color: "red"
    };

    function showError() {
        if (errorVisible) {
            return <p style={errorStyle}>The email address or password was incorrect.</p>;
        }
    }

    return (
        <div className="LoginInputContainer">
            {showError()}
            <div className="Inputs">
                <EmailAddressInput emailAddress={emailAddress} onEmailChange={onEmailAddressChange}/>
            </div>
            <div className="Inputs">
                <PasswordInput password={password} onPasswordChange={onPasswordChange}/>
            </div>
            <div className="Inputs">
                <Button variant="contained" onClick={onLoginClick}> Login </Button>
            </div>
            <p> Not a user? <Link to="/register"> Sign up! </Link></p>
            {redirect()}
        </div>
    );
}

export default withRouter(LoginPage);