import {Document, Types} from "mongoose";

export interface iCharacter extends Document {
    userId: Types.ObjectId;
    created: Date;
    name: string;
    money: number;
    mainHand: Types.ObjectId | null;
    offHand: Types.ObjectId | null;
    armor: Types.ObjectId | null;
    ring: Types.ObjectId | null;
}

export interface iEquipment extends iItem, Document {
    attack: number;
    defense: number;
    slot: number;
}

export interface iItem extends Document {
    name: string;
    description: string;
    value: number;
}

export interface iSession extends Document {
    sessionId: string;
    created: Date;
    userId: Types.ObjectId;
}

export interface iPost extends Document {
    postDate: Date;
    chatChannel: number;
    message: string;
}

export interface iUser extends Document {
    registered: Date;
    email: string;
    username: string;
    password: string;
    premiumCurrency: number;
}