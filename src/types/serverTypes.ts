export interface iMessage {
    chatNum: number;
    message: string;
}