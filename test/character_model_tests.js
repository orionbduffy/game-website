const chai = require('chai');
const expect = chai.expect;

const Mongoose = require('mongoose');

const {Character} = require('../build/server/models/character');

describe('Character model tests', function() {

    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });

    beforeEach(async function () {

        await Character.deleteMany({});
    });

    describe("Basic validation tests", function () {
        it("Should allow a valid name and ObjectId", async function () {

            const character = await new Character({
                userId: new Mongoose.Types.ObjectId(),
                name: "Hope Andreas Mikaelson"
            });

            expect(Mongoose.Types.ObjectId.isValid(character.userId)).to.equal(true);
            expect(character.name).to.equal("Hope Andreas Mikaelson");

            const savedCharacter = await character.save();

            expect(Mongoose.Types.ObjectId.isValid(savedCharacter.userId)).to.equal(true);
            expect(savedCharacter.name).to.equal("Hope Andreas Mikaelson");
        });
    });

    describe("Name validation tests", function () {
        it("Should not allow invalid names", async function () {

            let character = await new Character({
                userId: new Mongoose.Types.ObjectId(),
                name: "Hope Andreas Mikaelson Jr"
            });

            try {
                const savedCharacter = await character.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Character validation failed: name: Invalid name. Name must be between " +
                    "5 and 30 characters, and only contain English letters and numbers. Up to 2 spaces are allowed");
            }

            character = await new Character({
                userId: new Mongoose.Types.ObjectId(),
                name: " Hope Mikaelson"
            });

            try {
                const savedCharacter = await character.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Character validation failed: name: Invalid name. Name must be between " +
                    "5 and 30 characters, and only contain English letters and numbers. Up to 2 spaces are allowed");
            }

            character = await new Character({
                userId: new Mongoose.Types.ObjectId(),
                name: "Hope Mikaelson Jr."
            });

            try {
                const savedCharacter = await character.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Character validation failed: name: Invalid name. Name must be between " +
                    "5 and 30 characters, and only contain English letters and numbers. Up to 2 spaces are allowed");
            }

            character = await new Character({
                userId: new Mongoose.Types.ObjectId(),
                name: "Hopeoiphjpoiuhjophpouihnpohuihpooihniopupghuihgbiuoipuhiousohiuehiosruhgijuoihuohpouhi Mikaelson"
            });

            try {
                const savedCharacter = await character.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Character validation failed: name: Invalid name. Name must be between " +
                    "5 and 30 characters, and only contain English letters and numbers. Up to 2 spaces are allowed");
            }

            character = await new Character({
                userId: new Mongoose.Types.ObjectId(),
                name: "Hope"
            });

            try {
                const savedCharacter = await character.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Character validation failed: name: Invalid name. Name must be between " +
                    "5 and 30 characters, and only contain English letters and numbers. Up to 2 spaces are allowed");
            }
        });
    });
});