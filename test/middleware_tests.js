const mocha = require("mocha");
const chai = require("chai");
const sinon = require("sinon");
const httpMock = require("node-mocks-http");
const Express = require("express");
const middleware = require("../build/server/middleware.js");
const Mongoose = require("mongoose");
const {Session} = require("../build/server/models/session");
const {User} = require("../build/server/models/user");
const Nanoid = require("nanoid");

const expect = chai.expect;

const {authenticate, strongParams} = middleware;
describe("strongParams tests", function()
{
    describe("Correct string checks", function () {
        it("Should make sure correct strings are allowed", function () {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {emailAddress: "Steve@gmail.com", username: "Steve Jobs", password: ""}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            strongParams({emailAddress: 'string', username: 'string', password: 'string'})(request, response, next);
            const {emailAddress, username, password} = response.locals.strongParams;

            expect(next.calledOnce).to.equal(true);
            expect(emailAddress).to.equal("Steve@gmail.com");
            expect(username).to.equal("Steve Jobs");
            expect(password).to.equal("");
        });
    });

    describe("Correct number checks", function ()
    {
        it("Should make sure correct numbers are allowed", function () {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {number: 10, number2: 0, number3: -999, number4: Infinity, number5: NaN}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            strongParams({
                number: 'number',
                number2: 'number',
                number3: 'number',
                number4: "number",
                number5: "number"
            })(request, response, next);
            const {number, number2, number3, number4, number5} = response.locals.strongParams;

            expect(next.calledOnce).to.equal(true);
            expect(number).to.equal(10);
            expect(number2).to.equal(0);
            expect(number3).to.equal(-999);
            expect(number4).to.equal(Infinity);
            expect(number5).to.be.NaN;
        });
    });

    describe("Correct array checks", function ()
    {
        it("Should make sure correct arrays are allowed", function () {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {array: [], array2: ["pie"], array3: [10, 11, 27, 0], array4: [false]}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            strongParams({
                array: 'array',
                array2: ' array',
                array3: 'Array',
                array4: "array"
            })(request, response, next);
            const {array, array2, array3, array4} = response.locals.strongParams;

            expect(next.calledOnce).to.equal(true);
            expect(array).to.eql([]);
            expect(array2).to.eql(["pie"]);
            expect(array3).to.eql([10, 11, 27, 0]);
            expect(array4).to.eql([false]);
        });
    });

    describe("Correct boolean checks", function ()
    {
        it("Should make sure correct booleans are allowed", function () {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {boolean: true, boolean2: false}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            strongParams({
                boolean: 'boolean',
                boolean2: 'boolean'
            })(request, response, next);
            const {boolean, boolean2} = response.locals.strongParams;
            expect(next.calledOnce).to.equal(true);
            expect(boolean).to.equal(true);
            expect(boolean2).to.equal(false);
        });
    });

    describe("Programmer fail checks", function()
    {
        it("Should fail if the programmer makes an error", function ()
        {
            let request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {boolean: true}
                });

            let response = httpMock.createResponse();
            let next = sinon.spy();

            strongParams({
                boolean2: 'boolean'
            })(request, response, next);
            let {boolean} = response.locals.strongParams;

            expect(response.statusCode).to.equal(400);
            expect(boolean).to.equal(undefined);

            request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {boolean: true}
                });
            response = httpMock.createResponse();
            next = sinon.spy();

            strongParams({
                boolean: 'boolean2'
            })(request, response, next);
            boolean = response.locals.strongParams.boolean;

            expect(response.statusCode).to.equal(400);
            expect(boolean).to.equal(undefined);
        });
    });

    describe("Hack fail checks", function()
    {
        it("Should fail if someone tries to send things that should not be allowed", function ()
        {
            let request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {boolean: {}}
                });

            let response = httpMock.createResponse();
            let next = sinon.spy();

            strongParams({
                boolean: 'boolean'
            })(request, response, next);
            let {boolean} = response.locals.strongParams;

            expect(response.statusCode).to.equal(400);
            expect(boolean).to.equal(undefined);

            request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/user",
                    body: {boolean: "Cake"}
                });

            response = httpMock.createResponse();
            next = sinon.spy();

            strongParams({
                boolean: 'boolean'
            })(request, response, next);
            boolean = response.locals.strongParams.boolean;

            expect(response.statusCode).to.equal(400);
            expect(boolean).to.equal(undefined);
        });
    });
});

describe("Authentication middleware checks", function ()
{
    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

        User.deleteMany();
        Session.deleteMany();

        await User.create({email: "john@gmail.com", username:"Johnson1000",
            password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});
        await User.create({email: "sam@gmail.com", username:"Sam1000",
            password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8h"});
        await User.create({email: "steve@gmail.com", username:"Steve1000",
            password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8i"});

        let user = await User.findOne({ email: "john@gmail.com" });
        await Session.create({sessionId: "AaBbCcDdFfGgHhIiJjKkLlMm", userId: user._id});

        user = await User.findOne({ email: "sam@gmail.com" });
        await Session.create({sessionId: "AaBbCcDdFfGgHhIiJjKkLlM0", userId: user._id});

        user = await User.findOne({ email: "steve@gmail.com" });
        await Session.create({sessionId: "AaBbCcDdFfGgHhIiJjKkLlM1", userId: user._id});
    });

    describe("Valid sessionIds check", async function()
    {
        it("Should make sure valid sessionIds get authenticated", async function ()
        {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/posts",
                    cookies: {sessionId: "AaBbCcDdFfGgHhIiJjKkLlMm", secret: "pie"}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            await authenticate(request, response, next);

            expect(next.calledOnce).to.equal(true);
        });
    });

    describe("nonexistent sessionIds check", function()
    {
        it("Should make sure nonexistent sessionIds do not get authenticated", async function ()
        {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/posts",
                    cookies: {sessionId: "AaBbCcDdFfGgHhIiJjKkLlM3"}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            await authenticate(request, response, next);

            expect(next.calledOnce).to.equal(false);
            expect(response.statusCode).to.equal(401);
        });
    });

    describe("object sessionIds check", function()
    {
        it("Should make sure object sessionIds do not get authenticated", async function ()
        {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/posts",
                    cookies: {sessionId: {pie: "AaBbCcDdFfGgHhIiJjKkLlM0"}}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            try
            {
                await authenticate(request, response, next);

                expect.fail;
            }
            catch (error)
            {
                expect.success;
            }
        });
    });

    describe("null sessionIds check", function()
    {
        it("Should make sure null sessionIds do not get authenticated", async function ()
        {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/posts",
                    cookies: {sessionId: null}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            try
            {
                await authenticate(request, response, next);

                expect.fail;
            }
            catch (error)
            {
                expect.success;
            }
        });
    });

    describe("undefined sessionIds check", function()
    {
        it("Should make sure undefined sessionIds do not get authenticated", async function ()
        {
            const request = httpMock.createRequest(
                {
                    method: 'POST',
                    url: "/posts",
                    cookies: {sessionId: undefined}
                });

            const response = httpMock.createResponse();
            const next = sinon.spy();

            try
            {
                await authenticate(request, response, next);

                expect.fail;
            }
            catch (error)
            {
                expect.success;
            }
        });
    });
});