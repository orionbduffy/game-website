const chai = require('chai');
const expect = chai.expect;

const Nanoid = require('nanoid');
const Mongoose = require('mongoose');

const {Session} = require('../build/server/models/session');

describe('Session model tests', function() {

    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });

    beforeEach(async function () {

        await Session.deleteMany({});
    });

    describe("Basic validation tests", function () {
        it("Should allow a valid sessionId and ObjectId", async function () {

            const session = await new Session({
                sessionId: Nanoid(24),
                userId: new Mongoose.Types.ObjectId()
            });

            expect(Mongoose.Types.ObjectId.isValid(session.userId)).to.equal(true);
            expect(session.sessionId).to.be.a("string");
            expect(session.sessionId.length).to.equal(24);

            const savedSession = await session.save();

            expect(Mongoose.Types.ObjectId.isValid(savedSession.userId)).to.equal(true);
            expect(savedSession.sessionId).to.be.a("string");
            expect(savedSession.sessionId.length).to.equal(24);
        });
    });

    describe("sessionId validation tests", function () {
        it("Should not allow invalid sessionIds", async function () {

            let session = await new Session({
                sessionId: Nanoid(25),
                userId: new Mongoose.Types.ObjectId()
            });

            try {
                const savedSession = await session.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Session validation failed: sessionId: Invalid sessionId. sessionId must be " +
                    "24 characters, and only contain English letters and numbers, as well as dashes and underscores.");
            }

            session = await new Session({
                sessionId: Nanoid(23) + " ",
                userId: new Mongoose.Types.ObjectId()
            });

            try {
                const savedSession = await session.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Session validation failed: sessionId: Invalid sessionId. sessionId must be " +
                    "24 characters, and only contain English letters and numbers, as well as dashes and underscores.");
            }

            session = await new Session({
                sessionId: Nanoid(23) + ".",
                userId: new Mongoose.Types.ObjectId()
            });

            try {
                const savedSession = await session.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Session validation failed: sessionId: Invalid sessionId. sessionId must be " +
                    "24 characters, and only contain English letters and numbers, as well as dashes and underscores.");
            }

            session = await new Session({
                sessionId: Nanoid(23),
                userId: new Mongoose.Types.ObjectId()
            });

            try {
                const savedSession = await session.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("Session validation failed: sessionId: Invalid sessionId. sessionId must be " +
                    "24 characters, and only contain English letters and numbers, as well as dashes and underscores.");
            }
        });
    });
});