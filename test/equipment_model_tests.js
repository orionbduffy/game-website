const chai = require('chai');
const expect = chai.expect;

const Nanoid = require('nanoid');
const Mongoose = require('mongoose');

const {Equipment} = require('../build/server/models/equipment');

describe('Equipment model tests', function() {

    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });

    beforeEach(async function () {

        await Equipment.deleteMany({});
    });

    describe("Basic validation tests", function () {
        it("Should allow a valid name and slot, and have defaults for the rest", async function () {

            const equipment = await new Equipment({
                name: "Sword",
                slot: 0
            });

            expect(equipment.name).to.equal("Sword");
            expect(equipment.description).to.equal("No description available");
            expect(equipment.value).to.equal(1);
            expect(equipment.attack).to.equal(0);
            expect(equipment.defense).to.equal(0);
            expect(equipment.slot).to.equal(0);

            const savedEquipment = await equipment.save();

            expect(savedEquipment.name).to.equal("Sword");
            expect(savedEquipment.description).to.equal("No description available");
            expect(savedEquipment.value).to.equal(1);
            expect(savedEquipment.attack).to.equal(0);
            expect(savedEquipment.defense).to.equal(0);
            expect(savedEquipment.slot).to.equal(0);
        });
    });

    describe("Name validation tests", function () {
        it("Should not allow invalid names", async function () {

            let equipment = await new Equipment({
                name: "Sword",
                slot: 4
            });

            try {
                const savedEquipment = await equipment.save();

                expect.fail("Expected error not thrown");

            } catch (error) {

                expect(error.message).to.equal("Equipment validation failed: slot: Above equipment slot range");
            }

            equipment = await new Equipment({
                name: "Sword",
                slot: -1
            });

            try {
                const savedEquipment = await equipment.save();

                expect.fail("Expected error not thrown");

            } catch (error) {

                expect(error.message).to.equal("Equipment validation failed: slot: Below equipment slot range");
            }
        });
    });

    describe("Slot Type virtual attribute test", function () {
        it("Should allow setting the slot number using the type, and getting the slot type as a string", async function () {

            const equipment = await new Equipment({
                name: "Sword",
                slot: 0
            });

            equipment.slotType = 'mainHand';
            expect(equipment.slot).to.equal(0);
            expect(equipment.slotType).to.equal('mainHand');

            equipment.slotType = 'offHand';
            expect(equipment.slot).to.equal(1);
            expect(equipment.slotType).to.equal('offHand');

            equipment.slotType = 'armor';
            expect(equipment.slot).to.equal(2);
            expect(equipment.slotType).to.equal('armor');

            equipment.slotType = 'ring';
            expect(equipment.slot).to.equal(3);
            expect(equipment.slotType).to.equal('ring');



            try {
                equipment.slotType = 'pie';

                expect.fail("Expected error not thrown");

            } catch (error) {

                expect(error.message).to.equal("Not a valid slot type");
            }
        });
    });
});