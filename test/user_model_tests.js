const chai = require('chai');
const expect = chai.expect;

const Mongoose = require('mongoose');

const {User} = require('../build/server/models/user');

describe('User model tests', function() {

    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });

    beforeEach(async function () {

        await User.deleteMany({});
    });

    describe("Basic validation tests", function () {
        it("Should allow valid emails, usernames, and passwords", async function () {

            const user = await new User({email: "john@gmail.com", username:"Johnson1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            expect(user.email).to.equal("john@gmail.com");
            expect(user.username).to.equal("Johnson1000");
            expect(user.password).to.equal("$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m");

            const savedUser = await user.save();

            expect(savedUser.email).to.equal("john@gmail.com");
            expect(savedUser.username).to.equal("Johnson1000");
            expect(savedUser.password).to.equal("$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m");
        });
    });

    describe("Email validation tests", function () {
        it("Should not allow invalid emails", async function () {

            let user = await new User({email: "johngmail.com", username:"Johnson1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: email: Invalid email.");
            }

            user = await new User({email: "john@gmailcom", username:"Johnson1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: email: Invalid email.");
            }

            user = await new User({email: "@gmail.com", username:"Johnson1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: email: Invalid email.");
            }
        });
    });

    describe("Username validation tests", function () {
        it("Should not allow non alphanumeric usernames", async function () {

            let user = await new User({email: "john@gmail.com", username:"John son1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: username: Invalid username. Username must be between " +
                    "5 and 30 characters, and only contain English letters and numbers.");
            }

            user = await new User({email: "john@gmail.com", username:"Johnson!000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: username: Invalid username. Username must be between " +
                    "5 and 30 characters, and only contain English letters and numbers.");
            }

            user = await new User({email: "john@gmail.com", username:"JΩhnsΩn1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: username: Invalid username. Username must be between " +
                    "5 and 30 characters, and only contain English letters and numbers.");
            }

            user = await new User({email: "john@gmail.com",
                username:"Johnsohjiuyxdbryuiohrioudhgiofhdiufobdyifodhdiohdfoiysrhdgiuoifhbzyodhfbidohigofzdboiun1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: username: Invalid username. Username must be between " +
                    "5 and 30 characters, and only contain English letters and numbers.");
            }

            user = await new User({email: "john@gmail.com",
                username:"John",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: username: Invalid username. Username must be between " +
                    "5 and 30 characters, and only contain English letters and numbers.");
            }
        });
    });

    describe("Password validation tests", function () {
        it("Should not allow non-bcrypt-hash passwords", async function () {

            let user = await new User({email: "john@gmail.com", username:"Johnson1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: password: Invalid password. Password must be a Bcrypt hash.");
            }

            user = await new User({email: "john@gmail.com", username:"Johnson1000",
                password:"$2b$12$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfΩ1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: password: Invalid password. Password must be a Bcrypt hash.");
            }

            user = await new User({email: "john@gmail.com", username:"Johnson1000",
                password:"$2b$ab$GhvMmNVjRW29ulnudl.LbuAnUtN/LRfe1JsBm1Xu6LE3059z5Tr8m"});

            try {
                const savedUser = await user.save();

                expect.fail("Expected error not thrown");
            } catch (error) {

                expect(error.message).to.equal("User validation failed: password: Invalid password. Password must be a Bcrypt hash.");
            }
        });
    });
});